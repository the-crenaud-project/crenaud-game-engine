# The Crenaud Game Engine

Welcome to the CGE's gitlab !


## State of the project

The project is at an early state.

The first objective is to have an extremely basic version of the CGE to re-create CRENAUD 1 ( Catch Royal Edition Nazi : An Ultimate Dictator ) correctly.
The motor will then be enhanced when facing problems in CRENAUD 1 development.
As CRENAUD 1 doesn't have any physics specific feature, the motor will later be enhanced when working on another game ( probably CRENAUD 2 or 3 ).


## History

The CGE started as the compilation of Crenaud 2 and Crenaud 3 game engines.