package datastructures;

import javafx.geometry.Point2D;

/**
 * CGE's vector class
 * @author aethor
 * todo : 3+ dimension vectors ?
 */
public class Vector {

    private double x;
    private double y;

    public Vector(){
        this.x = 0;
        this.y = 0;
    }

    public Vector(double x, double y){
        this.x = x;
        this.y = y;
    }

    public Vector(Vector v){
        this.x = v.getX();
        this.y = v.getY();
    }


    /**
     *
     * @return the length of this vector
     */
    public double length(){
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }


    public double dot(Vector v){
        return (this.x * v.getX() + this.y * v.getY());
    }


    public Vector add(Vector v){
        return new Vector(this.x + v.x, this.y + v.y);
    }


    public Vector substract(Vector v){
        return new Vector(this.x - v.x, this.y - v.y);
    }

    public double distance(Vector v){
        return this.substract(v).length();
    }


    public boolean equals(Vector v){
        return (this.x == v.x && this.y == v.y);
    }

    public boolean isPerpendicular(Vector v){
        return (this.dot(v) == 0);
    }


    /**
     *
     * @param scalar
     * @return a new vector, corresponding to this one multiplied by the given scalar
     */
    public Vector getScaled(double scalar){
        return new Vector(this.x * scalar, this.y * scalar);
    }

    /**
     * Multiply every coordinate of this vector by the given scalar
     * @param scalar
     */
    public void scale(double scalar){
        this.x = this.x * scalar;
        this.y = this.y * scalar;
    }

    /**
     *
     * @return the reverse of this vector
     */
    public Vector getReverse(){
        return new Vector(- this.x, - this.y);
    }


    /**
     * reverse every coordinate of this vector
     */
    public void reverse(){
        this.x = - this.x;
        this.y = - this.y;
    }


    public Vector getUnitary(){
        return new Vector(this.x/this.length(), this.y/this.length());
    }

    public void unitarise(){
        this.x = this.x / this.length();
        this.y = this.y / this.length();
    }

    public Point2D toPoint2D(){
        return new Point2D(this.x, this.y);
    }


    public void setX(double x){
        this.x = x;
    }

    public void setY(double y){
        this.y = y;
    }

    public void setCoordinates(double x, double y){
        this.x = x;
        this.y = y;
    }

    public void setCoordinates(Vector v){
        this.x = v.getX();
        this.y = v.getY();
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public String toString(){
        return "(" + this.x + " , " + this.y + " )";
    }
}
