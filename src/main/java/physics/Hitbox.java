package physics;

import datastructures.Vector;

/**
 * Basic hitbox class ( rectangular for now )
 * @author aethor
 * todo : more hitboxes types
 */
public class Hitbox {

    private Vector position;
    private double width;
    private double height;

    private Hitbox(Vector position, double width, double height){
        this.position = position;
        this.width = width>=0?width:0;
        this.height = height>=0?height:0;
    }


    /**
     * check if two hitboxes are colliding geometrically
     * @param h
     * @return
     */
    public boolean isColliding(Hitbox h){
        return (getX() < h.getXMax()
            && getXMax() > h.getX()
            && getY() < h.getYMax()
            && getYMax() > h.getY());
    }


    public boolean equals(Hitbox h){
        return (width == h.width && height == h.height);
    }

    public String toString(){
        return "Hitbox : (" + getX() + "," + getY() + ")\n\twidth : " + width + "\n\theight : " + height + "\n";
    }


    public double getX(){
        return position.getX();
    }

    public double getY(){
        return position.getY();
    }

    public double getXMax(){
        return getX() + width;
    }

    public double getYMax(){
        return getY() + height;
    }

    public Vector getPosition() {
        return position;
    }

    public Vector getCenter(){
        return new Vector(getX() + width/2, getY() + height/2);
    }

    public void setPosition(Vector position) {
        this.position = position;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width>=0?width:0;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height>=0?height:0;
    }
}
