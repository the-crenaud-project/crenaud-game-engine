package physics;

import datastructures.Vector;
import javafx.scene.image.Image;
import physics.Force;
import physics.Hitbox;

import java.util.LinkedList;

/**
 * an abstract class representing a CGE's game object
 * @author aethor
 */
public abstract class GameObject {

    protected Vector position;
    protected Vector speed;
    protected Vector acceleration;
    protected Hitbox hitbox;

    protected double mass;
    protected LinkedList<Force> forces;

    protected Image sprite; //todo : change for an ANIMATION containing one sprite ( more logical )
    protected boolean visible;


    protected Vector lastFramePosition;
    protected boolean collidedLastFrame;

    /**
     * creates an empty object
     * @param position
     */
    public GameObject(Vector position){
        this.position = (position.getX()>=0 && position.getY()>=0)?position:new Vector(0,0);
        this.sprite = null;
        this.hitbox = null;
        this.mass = 0;
        initialize();
    }

    /**
     * creates a game object with a sprite
     * @param position
     * @param sprite
     */
    public GameObject(Vector position, Image sprite){
        this.position = (position.getX()>=0 && position.getY()>=0)?position:new Vector(0,0);
        this.sprite = sprite;
        this.hitbox = null;
        this.mass = 0;
        initialize();
    }

    /**
     * creates a physical object, capable of colliding.
     * @param position
     * @param sprite
     * @param hitbox
     * @param mass the mass of the object. If mass <= 0, the object wont be affected by any force.
     */
    public GameObject(Vector position, Image sprite, Hitbox hitbox, double mass){
        this.position = (position.getX()>=0 && position.getY()>=0)?position:new Vector(0,0);
        this.sprite = sprite;
        this.hitbox = hitbox;
        this.mass = mass>=0?mass:0;
        initialize();
    }

    private void initialize(){
        this.speed = new Vector(0, 0);
        this.acceleration = new Vector(0, 0);
        this.forces = new LinkedList<>();
        this.visible = true;
        this.lastFramePosition = this.position;
        this.collidedLastFrame = false;
    }

    /**
     * function used to know if an object is colliding with an other one. Shouldn't be used manually, as collisions functions are automatically launched by world updates.
     * @param otherObject
     * @return
     */
    protected boolean isColliding(GameObject otherObject){
        return (this.hitbox != null && otherObject.hitbox != null && this.hitbox.isColliding(otherObject.hitbox));
    }



    protected void update(double frameDuration){
        if(mass != 0){
            Vector sumOfForces = new Vector(0,0);
            for(Force force : forces){
                sumOfForces = sumOfForces.add(force.getVector());
            }
            acceleration = sumOfForces.getScaled(1 / mass);
        }
        speed = speed.add(acceleration.getScaled(frameDuration));
        position = position.add(speed.getScaled(frameDuration));
    }

    protected abstract void customUpdate(double frameDuration);
    protected abstract void collide(GameObject otherObject);

    /**
     * used to teleport an object. Beware ! The new position should be safe to be for the object, which means he shouldn't collide with anything else at this position.
     * @param pos
     */
    protected void setPosition(Vector pos){
        if(pos.getX()>=0 && pos.getY()>=0){
            this.position = pos;
            this.hitbox.setPosition(pos);
        }
    }

}
