package physics;

import datastructures.Vector;

import java.util.LinkedList;

/**
 * Game world, used to manage collisions and physics
 * @author aethor
 */
public class GameWorld {

    //todo : find a better datastructures to optimise traversal and research
    private LinkedList<GameObject> objects;
    private LinkedList<GameObject> objectsToAdd;
    private LinkedList<GameObject> objectsToRemove;
    private double frameDuration;
    private Vector dimension;

    public GameWorld(double frameDuration, Vector dimension){
        this.objects = new LinkedList<>();
        this.frameDuration = frameDuration>0?frameDuration:0.03;
        this.dimension = dimension;
    }

    public void update(){

        objectsToAdd.forEach(entity -> {
            this.add(entity);
        });
        objectsToAdd = new LinkedList<>();

        objectsToRemove.forEach(entity -> {
            this.remove(entity);
        });
        objectsToRemove = new LinkedList<>();


        for(GameObject gameObject : objects){
            gameObject.lastFramePosition = gameObject.position;
            gameObject.update(frameDuration);
            gameObject.customUpdate(frameDuration);
        }

        for(GameObject gameObject : objects){
            for(GameObject secondGameObject : objects){
                if(gameObject.isColliding(secondGameObject)){
                    gameObject.collide(secondGameObject);
                    gameObject.collidedLastFrame = true;
                }
            }
        }

        for(GameObject gameObject : objects){
            if(gameObject.collidedLastFrame == true){
                gameObject.setPosition(gameObject.lastFramePosition);
                gameObject.collidedLastFrame = false;
            }
        }

    }

    /**
     * used to add an object to the world. The object will be added next frame, only if he's not colliding with any other object ( todo : improve by auto placing if colliding )
     * @param objectToAdd the GameObject to add
     */
    public void add(GameObject objectToAdd){
        boolean objectToAddColliding = false;
        for(GameObject object : objects){
            if(objectToAdd.isColliding(object))
                objectToAddColliding = true;
        }

        if(!objectToAddColliding)
            objectsToAdd.add(objectToAdd);
    }

    /**
     * used to remove an object from the world. The object will be removed next frame.
     * @param objectToRemove
     */
    public void remove(GameObject objectToRemove){
        objectsToRemove.add(objectToRemove);
    }
}
