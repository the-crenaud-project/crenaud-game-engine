package physics;

import datastructures.Vector;

/**
 * @author aethor
 */
public abstract class Force {

    /**
     * constructor for a force
     */
    public Force(){

    }

    public abstract Vector getVector();

}
